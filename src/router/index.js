import Vue from 'vue'
import Router from 'vue-router'
import Home from '@/components/Home'
import Justice from '@/components/Justice'
import BailiffsOffices from '@/components/BailiffsOffices'
import About from '@/components/About'
import Knowledge from '@/components/Knowledge'
import Career from '@/components/Career'
import TechnicalAssistance from '@/components/TechnicalAssistance'

Vue.use(Router)

export default new Router({
  mode: 'history',
  routes: [
    {
      path: '/',
      name: 'Home',
      component: Home,
      order: 1,
      meta: {
        visible: false,
        title: 'Strona główna'
      }
    },
    {
      path: '/wymiar-sprawiedliwosci',
      name: 'Justice',
      component: Justice,
      order: 2,
      meta: {
        visible: true,
        title: 'Wymiar sprawiedliwości'
      }
    },
    {
      path: '/kancelarie-komornicze',
      name: 'BailiffsOffices',
      component: BailiffsOffices,
      order: 3,
      meta: {
        visible: true,
        title: 'Kancelarie komornicze'
      }
    },
    {
      path: '/o-nas',
      name: 'About',
      component: About,
      order: 4,
      meta: {
        visible: true,
        title: 'O nas'
      }
    },
    {
      path: '/wiedza',
      name: 'Knowledge',
      component: Knowledge,
      order: 6,
      meta: {
        visible: true,
        title: 'Wiedza'
      }
    },
    {
      path: '/kariera',
      name: 'Career',
      component: Career,
      order: 5,
      meta: {
        visible: true,
        title: 'Kariera'
      }
    },
    {
      path: '/wsparcie-techniczne',
      name: 'TechnicalAssistance',
      component: TechnicalAssistance,
      order: 7,
      meta: {
        visible: true,
        title: 'Wsparcie techniczne'
      }
    }
  ],
  scrollBehavior (to, from, savedPosition) {
    return { x: 0, y: 0 }
  }
})
